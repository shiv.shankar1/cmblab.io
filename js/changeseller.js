function changesellers() {
    var img = document.getElementById('sellers').src;

    if (img.indexOf('Seller_green')) {
        document.getElementById('buyers').src = 'img/buyer.png';
        document.getElementById('sellers').src = 'img/seller.png';
        document.getElementById('traderss').src = 'img/traders.png';
        document.getElementById('farmBrokers').src = 'img/farm_broker.png';
        document.getElementById('Logistics').src = 'img/Logistic.png';
        document.getElementById('Finances').src = 'img/Finance.png';
        document.getElementById('buyersText').style.display = 'none'

        document.getElementById('traderssText').style.display = 'none'
        document.getElementById('farmBrokersText').style.display = 'none'
        document.getElementById('LogisticsText').style.display = 'none'
        document.getElementById('FinancesText').style.display = 'none'

        document.getElementById('sellersText').style.display = 'block'
    } else {
        document.getElementById('sellers').src = 'img/Seller_green.png';
    }
}
