(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice() + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset(1).top - 140)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 140
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    /* if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
		$("#mainNav").removeClass("navbar-shrink");
		} */
	};
	// Collapse now if page is not at top
	navbarCollapse();
	// Collapse the navbar when page is scrolled
	$(window).scroll(navbarCollapse);

	// Hide navbar when modals trigger
	$('.portfolio-modal').on('show.bs.modal', function(e) {
		$('.navbar').addClass('d-none');
	})
	$('.portfolio-modal').on('hidden.bs.modal', function(e) {
		$('.navbar').removeClass('d-none');
	})
  
	$(".container-fluid").click(function(){
		$(".justify-content-end").removeClass("show");
	});
	$(".nav-item").click(function(){
		$(".justify-content-end").removeClass("show");
	});
	
	// process Imag & Value_Proposition Imag
	if($(window).width() > 1024 ) {
		$("#processImag").attr("src", "img/process-img1349x430.png");
		$("#valuePropositionImag").attr("src", "img/Value_Proposition.png");
	} else if($(window).width() > 768) {
		$("#processImag").attr("src", "img/process-img1024x430.png");
		$("#valuePropositionImag").attr("src", "img/Value_Proposition1024.png");
	} else if($(window).width() > 600) {
		$("#processImag").attr("src", "img/process-img768x430.png");
		$("#valuePropositionImag").attr("src", "img/Value_Proposition768.png");
	}else if($(window).width() > 425) {
		$("#processImag").attr("src", "img/process-img600.png");
		$("#valuePropositionImag").attr("src", "img/Value_Proposition600.png");
	} else {
		$("#processImag").attr("src", "img/process-img425.png");
		$("#valuePropositionImag").attr("src", "img/Value_Proposition425.png");
	}

	// Download App
	$("#faqPageShow").hover(function(){
		$(".faqPage").attr("src", "img/faq_btn_hover.png");
	});

	$("#faq_btn").hover(function(){
		$(".faqPage").attr("src", "img/faq_btn.png");
	});

})(jQuery); // End of use strict
