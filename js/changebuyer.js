function changeuyers() {
    var img = document.getElementById('buyers').src;

    if (img.indexOf('buyer.png')) {
        document.getElementById('buyers').src = 'img/buyer_yellow.png';
        document.getElementById('sellers').src = 'img/Seller_green.png';
        document.getElementById('traderss').src = 'img/traders.png';
        document.getElementById('farmBrokers').src = 'img/farm_broker.png';
        document.getElementById('Logistics').src = 'img/Logistic.png';
        document.getElementById('Finances').src = 'img/Finance.png';
        document.getElementById('sellersText').style.display = 'none'

        document.getElementById('traderssText').style.display = 'none'
        document.getElementById('farmBrokersText').style.display = 'none'
        document.getElementById('LogisticsText').style.display = 'none'
        document.getElementById('FinancesText').style.display = 'none'

        document.getElementById('buyersText').style.display = 'block'
    } else {
        document.getElementById('buyers').src = 'img/buyer.png';
    }
}