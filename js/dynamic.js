$(document).ready(function(){
  // Stage for you
    $("#sellers").click(function(){
      $("#sellersText").show();
      $("img#sellers").attr({src: "img/seller.png", alt: "seller Image"});
      $("img#buyers").attr({src: "img/buyer.png", alt: "buyer Image"});
      $("img#traderss").attr({src: "img/traders.png", alt: "traders Image"});
      $("img#farmBrokers").attr({src: "img/farm_broker.png", alt: "farm_broker Image"});
      $("img#Logistics").attr({src: "img/Logistic.png", alt: "Logistic Image"});
      $("img#Finances").attr({src: "img/Finance.png", alt: "Finance Image"});
      $("#buyersText").hide();
      $("#traderssText").hide();
      $("#farmBrokersText").hide();
      $("#LogisticsText").hide();
      $("#FinancesText").hide();
    });

    $("#buyers").click(function(){
      $("#sellersText").hide();
      $("#buyersText").show();
      $("img#sellers").attr({src: "img/Seller_green.png", alt: "Seller_green Image"});
      $("img#buyers").attr({src: "img/buyer_yellow.png", alt: "buyer_yellow Image"});
      $("img#traderss").attr({src: "img/traders.png", alt: "traders Image"});
      $("img#farmBrokers").attr({src: "img/farm_broker.png", alt: "farm_broker Image"});
      $("img#Logistics").attr({src: "img/Logistic.png", alt: "Logistic Image"});
      $("img#Finances").attr({src: "img/Finance.png", alt: "Finance Image"});
      $("#traderssText").hide();
      $("#farmBrokersText").hide();
      $("#LogisticsText").hide();
      $("#FinancesText").hide();
    });

    $("#traderss").click(function(){
      $("#sellersText").hide();
      $("#buyersText").hide();
      $("#traderssText").show();
      $("img#sellers").attr({src: "img/Seller_green.png", alt: "Seller_green Image"});
      $("img#buyers").attr({src: "img/buyer.png", alt: "buyer Image"});
      $("img#traderss").attr({src: "img/Traders_yellow.png", alt: "Traders_yellow Image"});
      $("img#farmBrokers").attr({src: "img/farm_broker.png", alt: "farm_broker Image"});
      $("img#Logistics").attr({src: "img/Logistic.png", alt: "Logistic Image"});
      $("img#Finances").attr({src: "img/Finance.png", alt: "Finance Image"});
      $("#farmBrokersText").hide();
      $("#LogisticsText").hide();
      $("#FinancesText").hide();
    });

    $("#farmBrokers").click(function(){
      $("#sellersText").hide();
      $("#buyersText").hide();
      $("#traderssText").hide();
      $("#farmBrokersText").show();
      $("img#sellers").attr({src: "img/Seller_green.png", alt: "Seller_green Image"});
      $("img#buyers").attr({src: "img/buyer.png", alt: "buyer Image"});
      $("img#traderss").attr({src: "img/traders.png", alt: "traders Image"});
      $("img#farmBrokers").attr({src: "img/farm_broker_yellow.png", alt: "farm_broker_yellow Image"});
      $("img#Logistics").attr({src: "img/Logistic.png", alt: "Logistic Image"});
      $("img#Finances").attr({src: "img/Finance.png", alt: "Finance Image"});
      $("#LogisticsText").hide();
      $("#FinancesText").hide();
    });

    $("#Logistics").click(function(){
      $("#sellersText").hide();
      $("#buyersText").hide();
      $("#traderssText").hide();
      $("#farmBrokersText").hide();
      $("#LogisticsText").show();
      $("img#sellers").attr({src: "img/Seller_green.png", alt: "Seller_green Image"});
      $("img#buyers").attr({src: "img/buyer.png", alt: "buyer Image"});
      $("img#traderss").attr({src: "img/traders.png", alt: "traders Image"});
      $("img#farmBrokers").attr({src: "img/farm_broker.png", alt: "farm_broker Image"});
      $("img#Logistics").attr({src: "img/Logistic_yellow.png", alt: "Logistic_yellow Image"});
      $("img#Finances").attr({src: "img/Finance.png", alt: "Finance Image"});
      $("#FinancesText").hide();
    });

    $("#Finances").click(function(){
      $("#sellersText").hide();
      $("#buyersText").hide();
      $("#traderssText").hide();
      $("#farmBrokersText").hide();
      $("#LogisticsText").hide();
      $("#FinancesText").show();
      $("img#sellers").attr({src: "img/Seller_green.png", alt: "Seller_green Image"});
      $("img#buyers").attr({src: "img/buyer.png", alt: "buyer Image"});
      $("img#traderss").attr({src: "img/traders.png", alt: "traders Image"});
      $("img#farmBrokers").attr({src: "img/farm_broker.png", alt: "farm_broker Image"});
      $("img#Logistics").attr({src: "img/Logistic.png", alt: "Logistic Image"});
      $("img#Finances").attr({src: "img/finance_yellow.png", alt: "finance_yellow Image"});
    });


    // For Application Features: Button 1, 2, 3, 4 and Text change
    // Hover 
    $("#cart1").hover(function(){
	  $("#cartText1").show();
      $("img#cart1").attr({src: "img/selected_1.png", alt: "selected_1"});
      $("img#cart2").attr({src: "img/2_2.png", alt: "2 Image"});
      $("img#cart3").attr({src: "img/3.png", alt: "3 Image"});
      $("img#cart4").attr({src: "img/4.png", alt: "4 Image"});
      $("#cartText2").hide();
      $("#cartText3").hide();
	  $("#cartText4").hide();
	  $("#card-text1").show();
      $("#card-text2").hide();
	  $("#card-text3").hide();
      $("#card-text4").hide();
      $("img#applicationImage").attr({src: "img/img1.png", alt: "png"});
      
    });
    $("#cart2").hover(function(){
      $("#cartText1").hide();
      $("#cartText2").show();
      $("img#cart1").attr({src: "img/1_4.png", alt: "1 Image"});
      $("img#cart2").attr({src: "img/selected_2.png", alt: "selected_2 Image"});
      $("img#cart3").attr({src: "img/3.png", alt: "3 Image"});
      $("img#cart4").attr({src: "img/4.png", alt: "4 Image"});
      $("#cartText3").hide();
      $("#cartText4").hide();
      $("img#applicationImage").attr({src: "img/img2.png", alt: "png"});
	  $("#card-text1").hide();
      $("#card-text2").show();
	  $("#card-text3").hide();
      $("#card-text4").hide();
    });
    $("#cart3").hover(function(){
      $("#cartText1").hide();
      $("#cartText2").hide();
      $("#cartText3").show();
      $("img#cart1").attr({src: "img/1_4.png", alt: "1 Image"});
      $("img#cart2").attr({src: "img/2_2.png", alt: "2 Image"});
      $("img#cart3").attr({src: "img/selected_3.png", alt: "selected_3 Image"});
      $("img#cart4").attr({src: "img/4.png", alt: "4 Image"});
      $("#cartText4").hide();
      $("img#applicationImage").attr({src: "img/img3.png", alt: "png"});
	  $("#card-text1").hide();
      $("#card-text2").hide();
	  $("#card-text3").show();
      $("#card-text4").hide();
    });
    $("#cart4").hover(function(){
      $("#cartText1").hide();
      $("#cartText2").hide();
      $("#cartText3").hide();
      $("#cartText4").show();
      $("img#cart1").attr({src: "img/1_4.png", alt: "1 Image"});
      $("img#cart2").attr({src: "img/2_2.png", alt: "2 Image"});
      $("img#cart3").attr({src: "img/3.png", alt: "3 Image"});
      $("img#cart4").attr({src: "img/selected_4.png", alt: "selected_4 Image"});
      $("img#applicationImage").attr({src: "img/img4.png", alt: "png"});
	  $("#card-text1").hide();
      $("#card-text2").hide();
	  $("#card-text3").hide();
      $("#card-text4").show();
    });

    // Onclicked
    $("#cart1").click(function(){
      $("#cartText1").show();
      $("img#cart1").attr({src: "img/selected_1.png", alt: "selected_1"});
      $("img#cart2").attr({src: "img/2_2.png", alt: "2 Image"});
      $("img#cart3").attr({src: "img/3.png", alt: "3 Image"});
      $("img#cart4").attr({src: "img/4.png", alt: "4 Image"});
      $("#cartText2").hide();
      $("#cartText3").hide();
      $("#cartText4").hide();
	  $("#card-text1").show();
      $("#card-text2").hide();
	  $("#card-text3").hide();
      $("#card-text4").hide();
    });
    $("#cart2").click(function(){
      $("#cartText1").hide();
      $("#cartText2").show();
      $("img#cart1").attr({src: "img/1_4.png", alt: "1 Image"});
      $("img#cart2").attr({src: "img/selected_2.png", alt: "selected_2 Image"});
      $("img#cart3").attr({src: "img/3.png", alt: "3 Image"});
      $("img#cart4").attr({src: "img/4.png", alt: "4 Image"});
      $("#cartText3").hide();
      $("#cartText4").hide();
	  $("#card-text1").hide();
      $("#card-text2").show();
	  $("#card-text3").hide();
      $("#card-text4").hide();
    });
    $("#cart3").click(function(){
      $("#cartText1").hide();
      $("#cartText2").hide();
      $("#cartText3").show();
      $("img#cart1").attr({src: "img/1_4.png", alt: "1 Image"});
      $("img#cart2").attr({src: "img/2_2.png", alt: "2 Image"});
      $("img#cart3").attr({src: "img/selected_3.png", alt: "selected_3 Image"});
      $("img#cart4").attr({src: "img/4.png", alt: "4 Image"});
      $("#cartText4").hide();
	  $("#card-text1").hide();
      $("#card-text2").hide();
	  $("#card-text3").show();
      $("#card-text4").hide();
    });
    $("#cart4").click(function(){
      $("#cartText1").hide();
      $("#cartText2").hide();
      $("#cartText3").hide();
      $("#cartText4").show();
      $("img#cart1").attr({src: "img/1_4.png", alt: "1 Image"});
      $("img#cart2").attr({src: "img/2_2.png", alt: "2 Image"});
      $("img#cart3").attr({src: "img/3.png", alt: "3 Image"});
      $("img#cart4").attr({src: "img/selected_4.png", alt: "selected_4 Image"});
	  $("#card-text1").hide();
      $("#card-text2").hide();
	  $("#card-text3").hide();
      $("#card-text4").show();
    });

  });
  
